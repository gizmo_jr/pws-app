//
//  PopupUIView.swift
//  PWS_APP
//
//  Created by Jasper on 24/07/2019.
//  Copyright © 2019 Jasper. All rights reserved.
//

import UIKit

class PopupUIView: UIView {
    
    let BackgroundColor = UIColor(named: "Background")
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layer.cornerRadius = frame.height / 8
        layer.shadowOpacity = 0.25
        layer.shadowRadius = 7
        layer.shadowOffset = CGSize(width: 0, height: 10)
        
        backgroundColor = BackgroundColor
        
    }
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
