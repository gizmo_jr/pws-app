//
//  OpeningViewController.swift
//  PWS_APP
//
//  Created by Jasper on 02/08/2019.
//  Copyright © 2019 Jasper. All rights reserved.
//

import UIKit

class OpeningViewController: UIViewController {
    
    @IBAction func btnLogin(_ sender: UIButton) {
        let storyboardLog = UIStoryboard(name: "LoginRegister", bundle: nil)
        let logViewController = storyboardLog.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(logViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnRegister(_ sender: UIButton) {
        let storyboardReg = UIStoryboard(name: "LoginRegister", bundle: nil)
        let regViewController = storyboardReg.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.present(regViewController, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
