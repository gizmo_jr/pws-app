//
//  ViewController.swift
//  XcodeLoginExample
//
//  Created by Belal Khan on 29/05/17.
//  Copyright © 2017 Belal Khan. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController: UIViewController {
    
    //The login script url make sure to write the ip instead of localhost
    //you can get the ip using ifconfig command in terminal
    let URL_USER_LOGIN = "http://192.168.178.164/pws-app/v1/login.php"
    //let URL_USER_LOGIN = "http://127.0.0.1/pws-app/v1/login.php"
    
    //the defaultvalues to store user data
    let defaultValues = UserDefaults.standard
    
    //the connected views
    //don't copy instead connect the views using assistant editor
    
    @IBOutlet weak var labelMessage: UILabel!
    
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    @IBAction func toRegister(_ sender: UIButton) {
        
        //switching the screen
        let storyboard1 = UIStoryboard(name: "LoginRegister", bundle: nil)
        let RegisterViewController = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.present(RegisterViewController, animated: true, completion: nil)
   }
    
    
    
   
    
    //the button action function
    
   
    @IBAction func buttonLogin(_ sender: UIButton) {
        
        //getting the username and password
        let parameters: Parameters=[
            "username":textFieldUsername.text!,
            "password":textFieldPassword.text!
        ]
        
        //making a post request
        Alamofire.request(URL_USER_LOGIN, method: .post, parameters: parameters).responseJSON
            {
                response in
                //printing response
                print(response)
                
                //getting the json value from the server
                if let result = response.result.value {
                    let jsonData = result as! NSDictionary
                    
                    //if there is no error
                    if(!(jsonData.value(forKey: "error") as! Bool)){
                        
                        //getting the user from response
                        let user = jsonData.value(forKey: "user") as! NSDictionary
                        
                        //getting user values
                        let userId = user.value(forKey: "id") as! Int
                        let userName = user.value(forKey: "username") as! String
                        let userEmail = user.value(forKey: "email") as! String
                        let userPhone = user.value(forKey: "phone") as! String
                        
                        //saving user values to defaults
                        self.defaultValues.set(userId, forKey: "userid")
                        self.defaultValues.set(userName, forKey: "username")
                        self.defaultValues.set(userEmail, forKey: "useremail")
                        self.defaultValues.set(userPhone, forKey: "userphone")
                        
                       //let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        //switching the screen
                       // let storyboard = UIStoryboard(name: "Main", bundle: nil)
                      //let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                       //self.navigationController?.pushViewController(homeViewController, animated: true)
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        self.present(viewController, animated: true, completion: nil)
                      // self.dismiss(animated: false, completion: nil)
                    }else{
                        //error message in case of invalid credential
                        self.labelMessage.text = "Invalid username or password"
                    }
                    
              }
        }
}
    
        override func viewDidLoad() {
        super.viewDidLoad()
        //hiding the navigation button
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        self.navigationController?.isNavigationBarHidden = true
            
            func viewDidAppear() {
                if defaultValues.string(forKey: "username") != nil{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.present(homeViewController, animated: true, completion: nil)
                    
                    
                }
            }
            
            
           
    }
        // Do any additional setup after loading the view, typically from a nib.
        
        //if user is already logg   ed in switching to profile screen
      

 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

