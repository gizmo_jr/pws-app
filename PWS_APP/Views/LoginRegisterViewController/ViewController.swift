//*
//  ViewController.swift
//  PWS_APP
//
//  Created by Jasper on 29/07/2019.
//  Copyright © 2019 Jasper. All rights reserved.
//



import Alamofire
import UIKit

class ViewController: UIViewController {
    
    //Defined a constant that holds the URL for our web service
    let URL_USER_REGISTER = "http://192.168.178.164/pws-app/v1/register.php"
        //let URL_USER_REGISTER = "http://127.0.0.1/pws-app/v1/register.php"
    //View variables
    
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPhone: UITextField!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var registerButton: UIButton!
    
    //Button action
    
    @IBAction func toLogin(_ sender: UIButton) {
        let storyboardLogin = UIStoryboard(name: "LoginRegister", bundle: nil)
        let loginViewController = storyboardLogin.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(loginViewController, animated: true, completion: nil)
    }
    
    @IBAction func buttonRegister(_ sender: UIButton) {
    
    
    
    
        
        //creating parameters for the post request
        let parameters: Parameters=[
            "username":textFieldUsername.text!,
            "password":textFieldPassword.text!,
            "name":textFieldName.text!,
            "email":textFieldEmail.text!,
            "phone":textFieldPhone.text!
        ]
        
        //Sending http post request
        Alamofire.request(URL_USER_REGISTER, method: .post, parameters: parameters).responseJSON
            {
                response in
                //printing response
                print(response)
                
                //getting the json value from the server
                if let result = response.result.value {
                    
                    //converting it as NSDictionary
                    let jsonData = result as! NSDictionary
                    
                    //displaying the message in label
                    self.labelMessage.text = jsonData.value(forKey: "message") as! String?
                }
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizeButton(buttonName: registerButton)
        
        textFieldUsername.leftViewMode = .always
        let imageviewtext = UIImageView()
        imageviewtext.frame = CGRect(x: 0, y: 0, width:20, height: 20)
        let imagetext = UIImage(named: "BASIC_UI-09-512")
        imageviewtext.image = imagetext
        textFieldUsername.leftView = imageviewtext
        
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 100.0, height: 2.0))
       
        
        
        textFieldPassword.leftViewMode = .always
        textFieldPassword.leftView = leftView
        let imageviewtextPW = UIImageView()
        imageviewtextPW.contentMode = .scaleAspectFit
        imageviewtextPW.frame = CGRect(x: 0, y: 0, width:20, height: 25)
        let imagetextPW = UIImage(named: "password (1)")
        imageviewtextPW.image = imagetextPW
        textFieldPassword.leftView = imageviewtextPW
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func customizeButton(buttonName: UIButton) {
        buttonName.layer.borderWidth = 1
        buttonName.layer.borderColor = #colorLiteral(red: 0.2803019149, green: 0.6928510464, blue: 1, alpha: 1)
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


