//
//  HomeViewController.swift
//  PWS_APP
//
//  Created by Jasper on 23/07/2019.
//  Copyright © 2019 Jasper. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var labelUserName: UILabel!
    
  
    
    @IBAction func buttonLogout(_ sender: UIButton) {
    
    
        // na openen andere viewcontroller, gaat hij niet terug naar LoginViewController
        
        
        let alertLogout = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
        
        alertLogout.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertaction) in
            
        }))
        
        alertLogout.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { (alertaction) in
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            UserDefaults.standard.synchronize()
            
            //switching to login screen
            let storyboard2 = UIStoryboard(name: "LoginRegister", bundle: nil)
            let hometologViewController = storyboard2.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(hometologViewController, animated: true, completion: nil)
        }))
        
        self.present(alertLogout, animated: true)
        //removing values from default
       //UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        //UserDefaults.standard.synchronize()
        
        //switching to login screen
        //let storyboard2 = UIStoryboard(name: "LoginRegister", bundle: nil)
        //let hometologViewController = storyboard2.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        //self.present(hometologViewController, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //hiding back button
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        self.navigationController?.isNavigationBarHidden = true

       // let defaultValues = UserDefaults.standard
     //   if let name = defaultValues.string(forKey: "username"){
            //setting the name to label
          //  labelUserName.text = name
     //   }else{
            //send back to login view controller
        }
       



    @IBAction func cancelAbout(_ sender: UIButton) {
        dismiss(animated:true)
    }

}
    
   
   

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
 

 }
 */
