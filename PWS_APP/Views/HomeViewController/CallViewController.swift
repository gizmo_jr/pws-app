//
//  CallViewController.swift
//  PWS_APP
//
//  Created by Jasper on 02/08/2019.
//  Copyright © 2019 Jasper. All rights reserved.
//

import UIKit

class CallViewController: UIViewController {

    @IBOutlet weak var textFieldNumber: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func facetimeCall(_ sender: UIButton) {
        var url:NSURL = NSURL(string: "facetime://+31627444815")!
        UIApplication.shared.openURL(url as URL)
    }
    @IBAction func callPhone2(_ sender: UIButton) {
        var url:NSURL = NSURL(string: "tel://+31611719449")!
        UIApplication.shared.openURL(url as URL)
    }
    
    
    @IBAction func callPhone(_ sender: UIButton) {
        var url:NSURL = NSURL(string: "tel://+31627444815")!
        UIApplication.shared.openURL(url as URL)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
