//
//  ToDoTableViewCell.swift
//  PWS_APP
//
//  Created by Jasper on 23/07/2019.
//  Copyright © 2019 Jasper. All rights reserved.
//

import UIKit

class ToDoTableViewCell: UITableViewCell {

    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cardView.addShadowAndRoundedCorners()
        
   
        
    }
    
    func setup(todoModel: ToDoModel) {
        titleLabel.text = todoModel.title
    }


}
