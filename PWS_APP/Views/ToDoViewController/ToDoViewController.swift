//
//  ToDoViewController.swift
//  PWS_APP
//
//  Created by Jasper on 23/07/2019.
//  Copyright © 2019 Jasper. All rights reserved.
//

import UIKit

class ToDoViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var FloatingActionButton: UIButton!
    @IBOutlet var helpView: UIVisualEffectView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        
        tableView.backgroundColor = UIColor(red: 26, green: 46, blue: 49, alpha: 0)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        
        ToDoFunctions.readToDos(completion: { [weak self] in
                // Completion
            self?.tableView.reloadData()
             
        })
        
        if UserDefaults.standard.bool(forKey: "seenTaskHelp") == false {
        view.addSubview(helpView)
        helpView.frame = view.frame
        
        }
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
       
        
        
        // Provide an empty backBarButton to hide the 'Back' text present by default in the back button.
        let backBarButtton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButtton
       
        
    }; override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "toAddToDoSegue" {
                let popup = segue.destination as! AddToDoViewController
                popup.doneSaving = { [weak self] in
                    self?.tableView.reloadData()
                }
            }
        }
    
    
    @IBAction func closeHelpView(_ sender: AppUIButton) {
        UIView.animate(withDuration: 0.5, animations:
            {
                self.helpView.alpha = 0
        }) { (success) in
        
        self.helpView.removeFromSuperview()
            UserDefaults.standard.set(true, forKey: "seenTaskHelp")
        }
    }
  
}
    
       // addButton.createFloatingActionButton()
        



extension ToDoViewController: UITableViewDataSource, UITableViewDelegate {

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Data.todoModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as! ToDoTableViewCell
        
    
            
        cell.setup(todoModel: Data.todoModels[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let todo = Data.todoModels[indexPath.row]
        
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (contextualAction, view, actionPerformed: @escaping (Bool) -> ()) in
            
            let alert = UIAlertController(title: "Delete Task", message: "Are you sure you want to delete this task: \(todo.title)", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertaction) in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (alertaction) in
                ToDoFunctions.deleteToDo(index: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
                actionPerformed(true)
            }))
            
            self.present(alert, animated: true)
            
            
        }
        
        delete.image = UIImage(named: "x-mark-32")
        delete.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.3098039216, blue: 0.3411764706, alpha: 1)

        return UISwipeActionsConfiguration(actions: [delete])
    }
    
}

