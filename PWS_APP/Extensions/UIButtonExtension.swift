//
//  UIButtonExtension.swift
//  PWS_APP
//
//  Created by Jasper on 24/07/2019.
//  Copyright © 2019 Jasper. All rights reserved.
//

import UIKit

extension UIButton {
    func createFloatingActionButton() {
        backgroundColor = UIColor.white
        layer.cornerRadius = frame.height / 2
        layer.shadowOpacity = 0.25
        layer.shadowRadius = 7
        layer.shadowOffset = CGSize(width: 0, height: 15)
    }

}
