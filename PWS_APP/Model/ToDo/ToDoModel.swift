//
//  ToDoModel.swift
//  PWS_APP
//
//  Created by Jasper on 23/07/2019.
//  Copyright © 2019 Jasper. All rights reserved.
//

import Foundation

class ToDoModel {
    let id: UUID
    var title: String
    
    init(title: String) {
        id = UUID()
        self.title = title
    }
}
