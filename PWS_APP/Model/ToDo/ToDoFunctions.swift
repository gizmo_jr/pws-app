//
//  ToDoFunctions.swift
//  PWS_APP
//
//  Created by Jasper on 23/07/2019.
//  Copyright © 2019 Jasper. All rights reserved.
//

import Foundation

class ToDoFunctions {
    static func createToDo(todoModel: ToDoModel) {
        Data.todoModels.append(todoModel)
    }
    
    static func readToDos(completion: @escaping () -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
            if Data.todoModels.count == 0 {
                Data.todoModels.append(ToDoModel(title: "Boeken aanschaffen"))
                Data.todoModels.append(ToDoModel(title: "PWS verslag scrijven"))
            
            }
        }
            
            DispatchQueue.main.async {
                completion()
            }
    
    }
    
    static func updateToDo(todoModel: ToDoModel) {
        
    }
    
    static func deleteToDo(index: Int) {
        Data.todoModels.remove(at: index)
        
    }
}

